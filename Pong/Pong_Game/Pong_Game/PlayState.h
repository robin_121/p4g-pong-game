#pragma once

#include "SFML/Graphics.hpp"
#include "Background.h"
#include "Paddels.h"
#include "Ball.h"
#include "Boundary.h"
#include "Score.h"

class Game;

/*
The main pong game state, two players
*/
class PlayState
{
public:
	PlayState();
	~PlayState();
	//initialise sprites and initial positions, we pass a pointer 
	//to the Game class to access its update and render funcitons
	void initPlayMode(Game*);
	void render();
	void update();
	//retreive start position from previous state
	void startPos();
	void resetScore();
	void setPlayerNames();
private:
	Game *PSgame;
	sf::Texture backgroundTex;	//background
	sf::Texture ballTex;
	Background backgroundSprite;
	Paddels paddelL; //left paddel
	Paddels paddelR; //right paddel
	int paddelLeft =0, paddelRight =1;
	Ball ball;
	Boundary boundary;
	Score scoreL;
	Score scoreR;
	sf::Text p1Name;
	sf::Text p2Name;
};

