#include "Paddels.h"
#include "Constants.h"
#include <math.h>
#include "Application.h"

void Paddels::update(int paddelNo, Boundary& boundary, Ball& ball)
{
	//left paddel
	if (paddelNo == 0)
	{	
		paddelYpos = GetPos().y;
		float incL = 0;

		ballPaddelCollision(paddelNo, ball);

		//if not at bottom of screen
		if (!((paddelYpos + GetSize().y) > (boundary.GetPos().y + boundary.GetSize().y)))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) 
			{
				incL = CONSTANTS::PADDEL_SPEED;
			}
		}
		else
		{
			SetPos(Vec2f{ CONSTANTS::LEFT_PADDEL_XPOS, (paddelYpos - 0.1f) });
		}

		//if not at top of screen
		if (!(paddelYpos < boundary.GetPos().y))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) 
			{
				incL = -CONSTANTS::PADDEL_SPEED;
			}
		}
		else
		{
			SetPos(Vec2f{ CONSTANTS::LEFT_PADDEL_XPOS, (paddelYpos + 0.1f) });
		}
		SetPos(Vec2f{ CONSTANTS::LEFT_PADDEL_XPOS, (paddelYpos + incL * Application::GetElapsedSecs()) });
	}

	//right paddel
	if (paddelNo == 1)
	{
		paddelYpos = GetPos().y;
		float incR = 0;

		ballPaddelCollision(paddelNo, ball);

		if (!(paddelYpos < boundary.GetPos().y))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) 
			{
				incR = -CONSTANTS::PADDEL_SPEED;
			}
		}
		else
		{
			SetPos(Vec2f{ CONSTANTS::RIGHT_PADDEL_XPOS, (paddelYpos + 0.1f) });
		}

		//if not at bottom of screen
		if (!((paddelYpos + GetSize().y) > (boundary.GetPos().y + boundary.GetSize().y)))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) 
			{
				incR = CONSTANTS::PADDEL_SPEED;
			}
		}
		else
		{
			SetPos(Vec2f{ CONSTANTS::RIGHT_PADDEL_XPOS, (paddelYpos - 0.1f) });
		}
		SetPos(Vec2f{ CONSTANTS::RIGHT_PADDEL_XPOS, (paddelYpos + incR * Application::GetElapsedSecs()) });
	}
}

void Paddels::ballPaddelCollision(int paddelNo, Ball& ball)
{
	ballXpos = ball.GetPos().x;
	ballYpos = ball.GetPos().y;
	paddelYpos = GetPos().y;
	paddelHieght = GetSize().y;

	//left paddel
	if (paddelNo == 0)
	{
		/*if within the paddels x range*/
		if ((ballXpos < CONSTANTS::LEFT_PADDEL_XPOS) && (ballXpos > CONSTANTS::LEFT_PADDEL_XPOS - GetSize().x))
		if (ballXpos <= CONSTANTS::LEFT_PADDEL_XPOS)
		{
			//if within the paddels y range
			if ((ballYpos > paddelYpos) && (ballYpos < paddelYpos + paddelHieght))
			{
				//find where the ball intersects relative to the centre of the paddel
				relativeBallintersect = ((paddelYpos + (paddelHieght / 2)) - ballYpos);
				normalisedRBI = (relativeBallintersect / (paddelHieght / 2));
				bounceAngle = normalisedRBI * CONSTANTS::MAX_BOUNCEANGLE;
				ball.SetPos(Vec2f{ CONSTANTS::LEFT_PADDEL_XPOS + GetSize().x + 1, ballYpos });
				ball.SetVel(Vec2f{ cosf(bounceAngle) * Application::GetElapsedSecs() * CONSTANTS::BALL_SPEED, -sinf(bounceAngle) * Application::GetElapsedSecs() * CONSTANTS::BALL_SPEED });
			}
		}
	}

	//right paddel
	if (paddelNo == 1)
	{
		//if within the paddels x range
		if (ballXpos + ball.GetSize().x >= CONSTANTS::RIGHT_PADDEL_XPOS)
		{
			//if within the paddels y range
			if ((ballYpos > paddelYpos) && (ballYpos < paddelYpos + paddelHieght))
			{
				//find where the ball intersects relative to the centre of the paddel
				relativeBallintersect = ((paddelYpos + (paddelHieght / 2)) - ballYpos);
				normalisedRBI = (relativeBallintersect / (paddelHieght / 2));
				bounceAngle = normalisedRBI * CONSTANTS::MAX_BOUNCEANGLE;
				ball.SetPos(Vec2f{ CONSTANTS::RIGHT_PADDEL_XPOS - ball.GetSize().x - 1, ballYpos });
				ball.SetVel(Vec2f{ -cosf(bounceAngle) * Application::GetElapsedSecs() * CONSTANTS::BALL_SPEED, -sinf(bounceAngle) * Application::GetElapsedSecs() * CONSTANTS::BALL_SPEED });
			}
		}
	}
}