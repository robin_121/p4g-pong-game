#include "Score.h"


void Score::update(int side)
{
	//left side
	if (side == 0)
	{
		scoreLeftInt += 1;
		scoreLeftString = std::to_string(scoreLeftInt);
		SetString(scoreLeftString);
	}

	//right side
	if (side == 1)
	{
		scoreRightInt += 1;
		scoreRightString = std::to_string(scoreRightInt);
		SetString(scoreRightString);
	}

}

std::string Score::getLeftScore()
{
	return scoreLeftString;
}

std::string Score::getRightScore()
{
	return scoreRightString;
}

void Score::resetScore()
{
	scoreRightInt = 0;
	scoreLeftInt = 0;
	scoreLeftString = std::to_string(scoreLeftInt);
	scoreRightString = std::to_string(scoreRightInt);
	SetString(scoreLeftString);
	SetString(scoreRightString);
}