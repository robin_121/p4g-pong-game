#pragma once

#include "SFML/Graphics.hpp"
#include "GameObj.h"
#include "Vec2f.h"


class Ball : public GameObj
{
public:
	Ball() : GameObj() {} ;
	~Ball() {};
	void update();
	void addInitialVelocity();
};

