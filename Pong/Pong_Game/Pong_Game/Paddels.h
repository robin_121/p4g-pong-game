#pragma once

#include "SFML/Graphics.hpp"
#include "GameObj.h"
#include "Boundary.h"
#include "Ball.h"

class Paddels : public GameObj
{
public:
	Paddels() : GameObj() {};
	~Paddels() {};
	void update(int, Boundary&, Ball&);
	void ballPaddelCollision(int, Ball&);
private:
	float relativeBallintersect;
	float normalisedRBI;
	float bounceAngle;
	float ballXpos;
	float ballYpos;
	float paddelYpos;
	float paddelHieght;
};

