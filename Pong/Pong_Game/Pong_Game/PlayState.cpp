#include "PlayState.h"
#include "Background.h"
#include "Ball.h"
#include "Paddels.h"
#include "Boundary.h"
#include "Game.h"
#include "Vec2int.h"
#include <cassert>

PlayState::PlayState()
{
}


PlayState::~PlayState()
{
}

void PlayState::initPlayMode(Game *game)
{
	PSgame = game;

	if (!backgroundTex.loadFromFile("pongSprites/PongBackground.png"))
		assert(false);

	if (!ballTex.loadFromFile("pongSprites/NewPongSprites.png"))
		assert(false);

	scoreL.SetFont(PSgame->GetData().font);
	scoreL.SetCharacterSize(40.0f);
	scoreL.SetTextPosition(Vec2f{ 530, 160 });
	scoreL.SetString("0");

	scoreR.SetFont(PSgame->GetData().font);
	scoreR.SetCharacterSize(40.0f);
	scoreR.SetTextPosition(Vec2f{ 610, 160 });
	scoreR.SetString("0");

	ball.SetTexture(ballTex, sf::IntRect(47, 55, 140, 127));
	ball.SetPos(Vec2f{ 600, 400 });
	ball.SetScale(Vec2f{ 0.15, 0.15 });

	paddelL.SetTexture(ballTex, sf::IntRect(204, 319, 36, 128));
	paddelL.SetPos(Vec2f{ 250, 330 });
	paddelL.SetScale(Vec2f{ 0.35, 1 });

	paddelR.SetTexture(ballTex, sf::IntRect(204, 319, 36, 128));
	paddelR.SetPos(Vec2f{ 900, 330 });
	paddelR.SetScale(Vec2f{ 0.35, 1 });

	backgroundSprite.SetTexture(backgroundTex);
	backgroundSprite.SetPos(Vec2f{ 0, 0 });

	boundary.SetTexture(ballTex, sf::IntRect(47, 55, 140, 127));
	boundary.SetScale(Vec2f{ 4.9, 4 });
	boundary.SetPos(Vec2f{ 230, 147 });

	PSgame->GetData().gdWindow;

}

void PlayState::setPlayerNames()
{
	p1Name = PSgame->getStartGameState()->getPlayerNames("p1");
	p2Name = PSgame->getStartGameState()->getPlayerNames("p2");
}

void PlayState::update()
{
	//update paddel movement
	paddelL.update(paddelLeft, boundary, ball);
	paddelR.update(paddelRight, boundary, ball);

	boundary.update(ball, PSgame, scoreL, scoreR);	//boundary calls score updates when required

	ball.update();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
	{
		PSgame->setState(Game::StateManager::PausedGame);
		PSgame->getPausedState()->setPausedString("- PRESS 'ENTER' TO CONTINUE -\n\n\n['P' TO PAUSE AT ANY TIME]\n\n\n['R' TO RESTART]\n\n\n['ESC' TO QUIT]");
	}
}

void PlayState::startPos()
{
	//get startGameState and it's positions of relevant sprites and set current positions of these sprites to their positions.
	paddelL.SetPos(PSgame->getStartGameState()->getStartingPositions("paddelL"));
	paddelR.SetPos(PSgame->getStartGameState()->getStartingPositions("paddelR"));
	ball.SetPos(PSgame->getStartGameState()->getStartingPositions("Ball"));
	ball.addInitialVelocity();
}

void PlayState::resetScore()
{
	scoreL.resetScore();
	scoreR.resetScore();
}

void PlayState::render()
{
	boundary.Render(*PSgame->GetData().gdWindow);
	backgroundSprite.Render(*PSgame->GetData().gdWindow);
	PSgame->GetData().gdWindow->draw(p1Name);
	PSgame->GetData().gdWindow->draw(p2Name);
	scoreL.RenderText(*PSgame->GetData().gdWindow);
	scoreR.RenderText(*PSgame->GetData().gdWindow);
	ball.Render(*PSgame->GetData().gdWindow);
	paddelL.Render(*PSgame->GetData().gdWindow);
	paddelR.Render(*PSgame->GetData().gdWindow); 
}


