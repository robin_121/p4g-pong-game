#pragma once

#include "SFML/Graphics.hpp"
#include "GameObj.h"

class Score : public GameObj
{
public:
	Score() : GameObj() {};
	~Score() {};
	void update(int);
	void resetScore();
	std::string getLeftScore();
	std::string getRightScore();
private:
	int scoreLeftInt;
	int scoreRightInt;
	std::string scoreLeftString;
	std::string scoreRightString;
};

