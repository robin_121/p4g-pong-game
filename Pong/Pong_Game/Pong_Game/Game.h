#pragma once
#include "SFML/Graphics.hpp"
#include "GameData.h"
#include "NameState.h"
#include "Background.h"
#include "PlayState.h"
#include "PausedState.h"
#include "StartGameState.h"

class Game
{
public:
	Game();
	~Game();

	enum StateManager
	{
		WaitingInit,
		FetchingName,
		StartGame,
		PlayingGame,
		PausedGame,
		Results
	};

	void initialise(sf::RenderWindow& );
	void update();
	void render();
	void textInput(char);
	void setState(StateManager);
	StateManager getPrevState() { return prevState; }
	StateManager getState() { return gState; }
	PausedState* getPausedState() { return &pauseds; }
	StartGameState* getStartGameState() { return &sgs; }
	PlayState* getPlayState() { return &ps; }
	GameData& GetData() { return GD; }

private:
	StateManager gState;
	StateManager prevState;
	GameData GD;
	NameState ns;
	PlayState ps;
	PausedState pauseds;
	StartGameState sgs;
	
};

