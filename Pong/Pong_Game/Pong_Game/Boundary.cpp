#include "Boundary.h"
#include "Game.h"


void Boundary::update(Ball& ball, Game *Bgame, Score& scoreL, Score& scoreR)
{
	//add collision detection here

	float velX = ball.GetVel().x;
	float velY = ball.GetVel().y;

	float ballX = ball.GetPos().x;
	float ballY = ball.GetPos().y;



	//if at top or bottom boundary
	if ((ball.GetPos().y < GetPos().y) || ((ball.GetPos().y + ball.GetSize().y) > (GetPos().y + GetSize().y)))
	{
		ball.SetVel(Vec2f{ velX, (velY *= -1) });
	}

	//if at left boundary
	if (ball.GetPos().x < GetPos().x)
	{
		//if at bouncable wall 
		if (!((ballY < GetPos().y + 370) && (ballY > GetPos().y + 115)))
		{
			ball.SetVel(Vec2f{ (velX *= -1), velY });
		}
		else //if within open goal space
		{
			scoreL.update(0);
			Bgame->getStartGameState()->setScore(scoreL.getLeftScore(), scoreR.getRightScore());
			Bgame->getStartGameState()->initialPositions();
			Bgame->setState(Game::StateManager::StartGame);
		}
	}


	//if at right boundary
	if (ball.GetPos().x > (GetPos().x + GetSize().x))
	{
		//if at bouncable wall 
		if (!((ballY < GetPos().y + 370) && (ballY > GetPos().y + 115)))
		{
			ball.SetVel(Vec2f{ (velX * -1), velY });
		}
		else //if within open goal space
		{
			scoreR.update(1);
			Bgame->getStartGameState()->setScore(scoreL.getLeftScore(), scoreR.getRightScore());
			Bgame->getStartGameState()->initialPositions();
			Bgame->setState(Game::StateManager::StartGame);
		}
	}
}
