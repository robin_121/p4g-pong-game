#pragma once

#include "SFML/Graphics.hpp"
#include "Game.h"


class Application
{
	public:
		Application();
		~Application();
		void run();

		static float GetElapsedSecs() { return sElapsedSecs.asSeconds(); }
	private:
		sf::RenderWindow gWindow;
		Game game;
		static sf::Time sElapsedSecs;	//track how much time each update/render takes for smooth motion
};

