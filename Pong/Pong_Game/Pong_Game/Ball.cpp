#include "Ball.h"


void Ball::update()
{
	SetPos(Vec2f{ GetPos().x + GetVel().x, GetPos().y + GetVel().y });
}


void Ball::addInitialVelocity()
{
	SetVel(Vec2f{ 0.1f, 0 });
}
