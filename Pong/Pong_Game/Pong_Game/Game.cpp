#include "Game.h"
#include "Constants.h"
#include "Background.h"
#include <cassert>


Game::Game()
{
	gState = StateManager::WaitingInit;
}


Game::~Game()
{
}


void Game::initialise(sf::RenderWindow& gWindow)
{
	GD.gdWindow = &gWindow; //asigning the 'pointer to a render window' (GD.gdwindow), to point at the game window we created in application
	gState = StateManager::FetchingName;

	if (!GD.font.loadFromFile("pongSprites/PressStart2P.ttf"))
		assert(false);

	//initialising states
	ns.init(this);
	pauseds.init(this);
	sgs.init(this);
	ps.initPlayMode(this);
}

void Game::update()
{
	switch (gState)
	{
	case Game::FetchingName:
		break;
	case Game::PlayingGame:
		ps.update();
		break;
	case Game::StartGame:
		sgs.update();
		break;
	case Game::PausedGame:
		pauseds.update();
		break;
	case Game::Results:
		break;
	default:
		break;
	}


}

void Game::render()
{
	switch (gState)
	{
	case Game::FetchingName:
		ns.render();
		break;
	case Game::PlayingGame:
		ps.render();
		break;
	case Game::StartGame:
		sgs.render();
		break;
	case Game::PausedGame:
		pauseds.render();
		break;
	case Game::Results:
		break;
	default:
		break;
	}
}



void Game::setState(StateManager state)
{
	prevState = gState;
	gState = state;
}



void Game::textInput(char keyEntered)
{
	switch (keyEntered)
	{
	case CONSTANTS::ESCAPE_KEY:
		GD.gdWindow->close();
	break;
	}

	if (gState == StateManager::FetchingName)
	{
		ns.textHandler(keyEntered);
	}

}