#include "GameObj.h"

void GameObj::Render(sf::RenderWindow& window) {
	window.draw(mSprite);
}

void GameObj::RenderText(sf::RenderWindow& window) {
	window.draw(mText);
}

void GameObj::SetTexture(const sf::Texture& t) {
	mSprite.setTexture(t, true);
}

void GameObj::SetTexture(const sf::Texture& t, const sf::IntRect& rect) {
	mSprite.setTexture(t);
	mSprite.setTextureRect(rect);
}

void GameObj::SetOrigin(const Vec2f& off) {
	mSprite.setOrigin(sf::Vector2f(off.x, off.y));
}

void GameObj::SetScale(const Vec2f& s) {
	mSprite.setScale(s.x, s.y);
}

Vec2f GameObj::GetScale() {
	return Vec2f{ mSprite.getScale().x, mSprite.getScale().y };
}

Vec2f GameObj::GetPos() {
	return Vec2f{ mSprite.getPosition().x,mSprite.getPosition().y };
}

void GameObj::SetPos(const Vec2f& pos) {
	mSprite.setPosition(pos.x, pos.y);
}

float GameObj::GetDegrees() {
	return mSprite.getRotation();
}

Vec2f GameObj::GetOrigin() {
	return Vec2f{ mSprite.getOrigin().x,mSprite.getOrigin().y };
}

void GameObj::SetDegrees(float angle) {
	mSprite.setRotation(angle);
}

void GameObj::AddRotation(float angle) {
	mSprite.rotate(angle);
}

void GameObj::SetVel(const Vec2f& v) {
	mVel.x = v.x;
	mVel.y = v.y;
}

const Vec2f GameObj::GetVel() const {
	return Vec2f{ mVel.x,mVel.y };
}

Vec2f GameObj::GetDim() const {
	return Vec2f{ (float)mSprite.getTextureRect().width, (float)mSprite.getTextureRect().height };
}

Vec2f GameObj::GetSize() {
	return Vec2f{ mSprite.getGlobalBounds().width, mSprite.getGlobalBounds().height };
}

void GameObj::SetString(const std::string String)
{
	mText.setString(String);
}

void GameObj::SetFont(const sf::Font& font)
{
	mText.setFont(font);
}

void GameObj::SetCharacterSize(const float size)
{
	mText.setCharacterSize(size * 0.75f);
}

void GameObj::SetTextPosition(const Vec2f& pos)
{
	mText.setPosition(pos.x, pos.y);
}