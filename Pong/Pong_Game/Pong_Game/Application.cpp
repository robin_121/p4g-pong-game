#include "Application.h"
#include "Constants.h"

sf::Time Application::sElapsedSecs;	//as it's static it need instantiating separately

Application::Application()
{
	gWindow.create(sf::VideoMode(CONSTANTS::SCREENRES.x, CONSTANTS::SCREENRES.y ), "PONG!");
	game.initialise(gWindow);
}


Application::~Application()
{
}

//the game loop
void Application::run()
{
	sf::Clock clock;
	while (gWindow.isOpen())
	{
		sf::Event event;
		while (gWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				gWindow.close();
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode < CONSTANTS::ASCIIRANGE)
				{
					game.textInput(static_cast<char>(event.text.unicode));
				}
					

			}
		}

		sElapsedSecs = clock.restart();

		gWindow.clear();
		game.update();
		game.render();
		gWindow.display();

	}

}