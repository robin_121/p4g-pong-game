#pragma once

#include "SFML/Graphics.hpp"

struct GameData
{
	sf::RenderWindow *gdWindow;		//for rendering, this is assigned in game
	sf::Font font;
	std::string playerName;
	std::string player1Name;
	std::string player2Name;
};