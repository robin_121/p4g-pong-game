#pragma once
#include "SFML/Graphics.hpp"
#include "Background.h"
#include "Ball.h"
#include "Paddels.h"
#include "Boundary.h"
#include "PausedState.h"
#include "Vec2int.h"
#include "Score.h"

class Game;
/*
The start state, before space bar is used to change state,
also where your return if restart 'R' is selected from pause menu.
*/
class StartGameState
{
public:
	StartGameState();
	~StartGameState();
	void init(Game*);
	void update();
	void render();
	void initialPositions();
	void setPlayerNames();
	void resetScore();
	void setScore(const std::string leftScore, const std::string rightScore);
	sf::Text getPlayerNames(std::string player);
	StartGameState* getStartGameState() { return this; }
	Vec2f getStartingPositions(std::string sprite);
private:
	Game* Sgame;
	sf::Texture BspriteTex;
	sf::Texture ballTex;
	Background backgroundSprite;
	Paddels paddelL;
	Paddels paddelR;
	Ball ball;
	Boundary boundary;
	Score scoreL;
	Score scoreR;
	int paddelLeft = 0, paddelRight = 1;
	sf::Text p1Name;
	sf::Text p2Name;
};

