#pragma once

#include "SFML/Graphics.hpp"
#include "GameObj.h"
#include "Ball.h"
#include "Vec2f.h"
#include "Score.h"

class Game;

class Boundary : public GameObj
{
public:
	Boundary() : GameObj() {};
	~Boundary() {};
	void update(Ball&, Game*, Score&, Score&);
private:
	Game *Bgame;
};