#pragma once

#include "Vec2f.h"
#include "SFML/Graphics.hpp"

/*
functions we can use for our sprites (game objects) and Text objects
*/

class GameObj
{
public:
	GameObj() : mVel{ 0, 0 } {};
	void Render(sf::RenderWindow& window);
	void RenderText(sf::RenderWindow& window);
	void SetTexture(const sf::Texture& t);
	void SetTexture(const sf::Texture& t, const sf::IntRect& rect);
	void SetOrigin(const Vec2f& off);
	void SetScale(const Vec2f& s);
	Vec2f GetScale();
	Vec2f GetPos();
	Vec2f GetOrigin();
	void SetPos(const Vec2f& pos);
	float GetDegrees();
	void SetDegrees(float angle);
	void AddRotation(float angle);
	void SetVel(const Vec2f& v);
	const Vec2f GetVel() const;
	Vec2f GetDim() const;
	Vec2f GetSize(); //for sprites that have been scaled
	void SetString(const std::string);
	void SetFont(const sf::Font& font);
	void SetCharacterSize(const float size);
	void SetTextPosition(const Vec2f& pos);
private:
	sf::Sprite mSprite; //image and position
	sf::Vector2f mVel;	//velocity
	sf::Text mText;
};

