#pragma once
#include "Vec2int.h"


//game data constant values

namespace CONSTANTS
{
	const Vec2int SCREENRES{ 1200, 800 };		//desired res
	const sf::Uint32 ASCIIRANGE{ 127 };			//range of ascii characters we care about
	const float LEFT_PADDEL_XPOS{ 250 };
	const float RIGHT_PADDEL_XPOS{ 900 };
	const int PADDEL_SPEED{ 450 };
	const float MAX_BOUNCEANGLE{ 1.309f }; //75 degrees in rads
	const float BALL_SPEED{ 600 };
	//ascii
	const char ENTER_KEY{ 13 };
	const char ESCAPE_KEY{ 27 };
	const char SPACE_KEY{ 32 };
	const char BACKSPACE_KEY{ 8 };
	const char P_KEY{ 8 };
	const char R_KEY{ 8 };
}