#include "Vec2f.h"
#include <math.h>


Vec2f::Vec2f() : x(0), y(0) {

}

Vec2f::Vec2f(float x_, float y_)
{
	x = x_;
	y = y_;
}

Vec2f Vec2f::operator* (const float scalar)
{
	return Vec2f(x * scalar, y * scalar);
}

Vec2f& Vec2f::operator+= (const Vec2f vec)
{
	this->x + vec.x;
	this->y + vec.y;
	return *this; // return a pointer to the now modified Vec2f;
}

void Vec2f::normalise()
{
	float Modulus = sqrt((x*x) + (y*y));
	x = x / Modulus;
	y = y / Modulus;
}