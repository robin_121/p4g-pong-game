#pragma once


class Vec2f
{
public:
	Vec2f();
	Vec2f(float x_, float y_);

	float x, y;

	Vec2f operator*(const float scalar);
	Vec2f& operator+=(const Vec2f vec);
	void normalise();
};
