#include "NameState.h"
#include "Constants.h"
#include "Game.h"



NameState::NameState()
{
}


NameState::~NameState()
{
}

void NameState::init(Game *game)
{
	Ngame = game;
}


void NameState::textHandler(char keyEntered)
{
	if (keyEntered == CONSTANTS::ENTER_KEY && Ngame->GetData().playerName.size() > 0)
	{
		if (Ngame->GetData().player1Name == "")
		{
			Ngame->GetData().player1Name = Ngame->GetData().playerName;
			Ngame->GetData().playerName = "";
			msg = "[ENTER TO CONTINUE]\n\n\nINSERT P1 NAME : " + Ngame->GetData().player1Name + "\n\n\nINSERT P2 NAME : ";
		}
		else
		{
			Ngame->GetData().player2Name = Ngame->GetData().playerName;
			Ngame->setState(Game::StateManager::PausedGame);
			Ngame->getStartGameState()->setPlayerNames();
			Ngame->getPausedState()->setPausedString("-- CLICK HERE TO PLAY --\n\n\n\n['P' TO PAUSE AT ANY TIME]\n\n\n['R' TO RESTART]\n\n\n['ESC' TO QUIT]");
		}
		
	}
	else if (keyEntered != (CONSTANTS::BACKSPACE_KEY) && keyEntered != (CONSTANTS::ENTER_KEY)
		&& keyEntered != (CONSTANTS::SPACE_KEY) && (Ngame->GetData().playerName.size() <= 10))
		{
			Ngame->GetData().playerName += keyEntered;
		}
		else  if (keyEntered == CONSTANTS::BACKSPACE_KEY && Ngame->GetData().playerName.size() > 0)
				{
					Ngame->GetData().playerName.erase(Ngame->GetData().playerName.size() - 1);
				}
}

void NameState::render()
{
	std::string string = msg + Ngame->GetData().playerName;
	sf::Text txt(string, Ngame->GetData().font, (20.0f * 0.75f));
	txt.setPosition(150, 330);
	Ngame->GetData().gdWindow->draw(txt);
}