#pragma once

#include "GameData.h"
#include "PausedState.h"

class Game;


class NameState
{
public:
	NameState();
	~NameState();

	void init(Game *game);
	void textHandler(char);
	void render();

private:
	Game* Ngame;
	std::string msg = "[ENTER TO CONTINUE]\n\n\nINSERT P1 NAME : ";
};

