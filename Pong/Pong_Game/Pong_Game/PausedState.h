#pragma once

#include "SFML/Graphics.hpp"
#include "GameData.h"

class Game;

class PausedState
{
public:
	PausedState();
	~PausedState();
	void init(Game*);
	void update();
	void render();
	void setPausedString(std::string);
private:
	Game* pgame;
	sf::Text PausedText;
};

