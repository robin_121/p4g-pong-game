#include "PausedState.h"
#include "Game.h"

PausedState::PausedState()
{
}


PausedState::~PausedState()
{
}

void PausedState::init(Game *game)
{
	pgame = game;
	PausedText.setFont(pgame->GetData().font);
	PausedText.setCharacterSize(20.0f * 0.75f);
	PausedText.setPosition(150, 330);
}

void PausedState::setPausedString(std::string ps) 
{ 
	PausedText.setString(ps);
}

void PausedState::update()
{
	if (pgame->getPrevState() == (Game::StateManager::FetchingName))
	{
		//mouse click in right area (over 'click here' text)
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			if (sf::Mouse::getPosition(*pgame->GetData().gdWindow).x > PausedText.getPosition().x + 30 && sf::Mouse::getPosition(*pgame->GetData().gdWindow).x < (PausedText.getPosition().x + PausedText.getLocalBounds().width - 60))
			{
				if (sf::Mouse::getPosition(*pgame->GetData().gdWindow).y > PausedText.getPosition().y - 15 && sf::Mouse::getPosition(*pgame->GetData().gdWindow).y < (PausedText.getPosition().y + PausedText.getLocalBounds().height) - 80)
				{
					pgame->setState(Game::StateManager::StartGame);
				}
			}
		}
	}

	if (pgame->getPrevState() == (Game::StateManager::StartGame))
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
		{
			pgame->setState(Game::StateManager::StartGame);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			pgame->getStartGameState()->initialPositions();
			pgame->setState(Game::StateManager::StartGame);
		}
	}

	if (pgame->getPrevState() == (Game::StateManager::PlayingGame))
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
		{
			pgame->setState(Game::StateManager::PlayingGame);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{

			pgame->getPlayState()->resetScore();
			pgame->getStartGameState()->resetScore();
			pgame->getStartGameState()->initialPositions();
			pgame->setState(Game::StateManager::StartGame);
		}
	}
}

void PausedState::render()
{
	pgame->GetData().gdWindow->draw(PausedText);
}