#include "StartGameState.h"
#include <cassert>
#include "Constants.h"
#include "Vec2f.h"

#include "Game.h"

StartGameState::StartGameState()
{
}


StartGameState::~StartGameState()
{
}


void StartGameState::init(Game *game)
{
	if (!BspriteTex.loadFromFile("pongSprites/PongBackground.png"))
		assert(false);

	if (!ballTex.loadFromFile("pongSprites/NewPongSprites.png"))
		assert(false);

	Sgame = game;

	scoreL.SetFont(Sgame->GetData().font);
	scoreL.SetCharacterSize(40.0f);
	scoreL.SetTextPosition(Vec2f{ 530, 160 });
	scoreL.SetString("0");

	scoreR.SetFont(Sgame->GetData().font);
	scoreR.SetCharacterSize(40.0f);
	scoreR.SetTextPosition(Vec2f{ 610, 160 });
	scoreR.SetString("0");

	ball.SetTexture(ballTex, sf::IntRect(47, 55, 140, 127));
	ball.SetPos(Vec2f{ 600, 400 });
	ball.SetScale(Vec2f{ 0.15f, 0.15f });

	paddelL.SetTexture(ballTex, sf::IntRect(204, 319, 36, 128));
	paddelL.SetPos(Vec2f{ 250, 330 });
	paddelL.SetScale(Vec2f{ 0.35f, 1.0f });

	paddelR.SetTexture(ballTex, sf::IntRect(204, 319, 36, 128));
	paddelR.SetPos(Vec2f{ 900, 330 });
	paddelR.SetScale(Vec2f{ 0.35f, 1.0f });

	backgroundSprite.SetTexture(BspriteTex);
	backgroundSprite.SetPos(Vec2f{ 0, 0 });

	boundary.SetTexture(ballTex, sf::IntRect(47, 55, 140, 127));
	boundary.SetScale(Vec2f{ 5, 4 });
	boundary.SetPos(Vec2f{ 235, 147 });

}

Vec2f StartGameState::getStartingPositions(std::string sprite)
{ 
	if (sprite == "Ball") return this->ball.GetPos();
	if (sprite == "paddelL") return this->paddelL.GetPos();
	if (sprite == "paddelR") return this->paddelR.GetPos();
}


void StartGameState::initialPositions()
{
	ball.SetPos(Vec2f{ 600, 400 });
	paddelL.SetPos(Vec2f{ 250, 330 });
	paddelR.SetPos(Vec2f{ 900, 330 });
}

void StartGameState::setPlayerNames()
{
	//to allign name in center
	if (!(Sgame->GetData().player1Name.size() == 10))
	{
		std::string spaces;
		int noSpaces = 10 - Sgame->GetData().player1Name.size();
		for (int i = 0; i <= noSpaces; i++)
		{
			spaces += " ";
		}
		p1Name.setString(spaces + Sgame->GetData().player1Name);
	}
	else
	{
		p1Name.setString(Sgame->GetData().player1Name);
	}
	p1Name.setFont(Sgame->GetData().font);
	p1Name.setCharacterSize(20.0f * 0.75f);
	p1Name.setPosition(400, 100);

	p2Name.setFont(Sgame->GetData().font);
	p2Name.setCharacterSize(20.0f * 0.75f);
	p2Name.setPosition(610, 100);
	p2Name.setString(Sgame->GetData().player2Name);
}

sf::Text StartGameState::getPlayerNames(std::string player)
{
	if (player == "p1")
		return p1Name;
	if (player == "p2")
		return p2Name;
}

void StartGameState::setScore(const std::string leftScore, const std::string rightScore)
{
	if (leftScore != "")
	{
		scoreL.SetString(leftScore);
	}
	if (rightScore != "")
	{
		scoreR.SetString(rightScore);
	}
}

void StartGameState::resetScore()
{
	scoreL.resetScore();
	scoreR.resetScore();
}

void StartGameState::update()
{
	//update ball and paddels
	//if space is pressed change state to playstate

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		Sgame->getPlayState()->setPlayerNames();
		Sgame->getPlayState()->startPos();
		Sgame->setState(Game::StateManager::PlayingGame);
	}
	
	paddelL.update(paddelLeft, boundary, ball);
	paddelR.update(paddelRight, boundary, ball);

	//set ball position to middle of left paddel
	ball.SetPos(Vec2f{ CONSTANTS::LEFT_PADDEL_XPOS + 15, paddelL.GetPos().y + 50 });

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
	{
		Sgame->setState(Game::StateManager::PausedGame);
		Sgame->getPausedState()->setPausedString("- PRESS 'ENTER' TO RETURN TO GAME -\n\n\n['P' TO PAUSE AT ANY TIME]\n\n\n['R' TO RESTART]\n\n\n['ESC' TO QUIT]");
	}
}


void StartGameState::render()
{
	boundary.Render(*Sgame->GetData().gdWindow);
	backgroundSprite.Render(*Sgame->GetData().gdWindow);
	Sgame->GetData().gdWindow->draw(p1Name);
	Sgame->GetData().gdWindow->draw(p2Name);
	scoreL.RenderText(*Sgame->GetData().gdWindow);
	scoreR.RenderText(*Sgame->GetData().gdWindow);
	ball.Render(*Sgame->GetData().gdWindow);
	paddelL.Render(*Sgame->GetData().gdWindow);
	paddelR.Render(*Sgame->GetData().gdWindow);
}